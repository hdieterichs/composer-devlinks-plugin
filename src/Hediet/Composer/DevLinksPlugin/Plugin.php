<?php

namespace Hediet\Composer\DevLinksPlugin;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Package\Link;
use Composer\Package\LinkConstraint\VersionConstraint;
use Composer\Plugin\PluginInterface;
use Hediet\Composer\DevLinks\DevLinksConfig;

class Plugin implements PluginInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        $vendorDir = $composer->getConfig()->get("vendor-dir");
        $composerLinksDevJson = $vendorDir . "/../composer.links.dev.json";
        if (file_exists($composerLinksDevJson))
        {
            $requires = $composer->getPackage()->getRequires();
            $requires[] = new Link("__root__", "hediet/composer-devlinks", new VersionConstraint("==", "0.1.1.0"));

            $config = DevLinksConfig::loadFromFile($composerLinksDevJson);
            $replacedPackages = $config->getReplacedPackages();
            foreach ($requires as $key => $link)
            {
                if (in_array($link->getTarget(), $replacedPackages))
                    unset($requires[$key]);
            }

            $composer->getPackage()->setRequires($requires);
        }
    }
}
