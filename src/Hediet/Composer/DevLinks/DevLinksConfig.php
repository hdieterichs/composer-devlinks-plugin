<?php

namespace Hediet\Composer\DevLinks;

class DevLinksConfig
{
    public static function loadFromFile($file)
    {
        $items = json_decode(file_get_contents($file), true);
        if ($items === null)
            throw new RuntimeException("'" . $file . "' is not a valid json file!");
        
        $includedFiles = array();
        $replacedPackages = array();
        
        if (isset($items["replaces"]))
        {
            foreach ($items["replaces"] as $key => $path)
            {
                $replacedPackages[] = $key;
                $includedFiles[] = self::processLink($path);
            }
        }
        
        if (isset($items["links"]))
        {
            foreach ($items["links"] as $path)
            {
                $includedFiles[] = self::processLink($path);
            }
        }
        
        return new DevLinksConfig($includedFiles, $replacedPackages);
    }
    
    private static function processLink($path)
    {
        if (strcmp(substr($path, -strlen(".php")), ".php") === 0)
            return $path;
        else
            return $path . "/vendor/autoload.php";
    }
    
    
    /**
     * @var string[]
     */
    private $replacedPackages;
    
    /**
     * @var string[]
     */
    private $includedFiles;

    private function __construct(array $includedFiles, array $replacedPackages)
    {
        
        $this->includedFiles = $includedFiles;
        $this->replacedPackages = $replacedPackages;
    }
    
    /**
     * @return string[]
     */
    public function getIncludedFiles()
    {
        return $this->includedFiles;
    }
    
    /**
     * @return string[]
     */
    public function getReplacedPackages()
    {
        return $this->replacedPackages;
    }
}